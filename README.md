SpaceMar!
=========

A [GAMES MADE QUICK??? FOUR](https://itch.io/jam/games-made-quick-four)
  [submission](https://itch.io/jam/games-made-quick-four/rate/545829).

-----------------------------------------------------

First things first. Congratulations on your scheduled promotion review! The only thing is left to decide is what exactly your new title will be.

The board is always prospecting new candidates and new market opportunities. There is some... stuff that our research team deemed worthy of further inspection. The stuff, they say, extends life. The stuff, we say, expands shareholder value.

With a budget assigned and a standard issue "Slate" Star Courier provided you will go on a business trip to acquire a sample. Feel free to pursue business opportunities along the way. Don't get too distracted, but don't forget to be awesome either.

### Instructions

Enter the planetary system, *click* a spot for initial warp-in and *fly* like your salary will depend on it!

### Controls

#### Ship

Left-hand side controls:

* `w`: Engage thrusters, take off or abort landing.
* `d`: Turn right/clockwise/starboard.
* `a`: Turn left/counter-clockwise/port.
* `~`: Dump stock in a durable cargo container.
* `Esc`: File for bankrupcy (only in space).

Right-hand side controls:

* `Up`: Engage thrusters, take off or abort landing.
* `Left`: Turn right/clockwise/starboard.
* `Right`: Turn left/counter-clockwise/port.
* `Right Ctrl`: Dump stock in a durable cargo container.
* `Backspace`: File for bankrupcy (only in space).

Mouse controls:

* TODO

#### UI

* `Shift+M`: Cycle markets
* `Shift+N`: Cycle nav lines
* `Shift+R`: Toggle planet rings
* `Shift+S`: Cycle score
* `Shift+T`: Cycle traces
* `t`: Cycle trajectories

#### Game/Technicals

* `Shift+G`: Toggle UI grid
* `Shift+LMB`: respawn
* `Shift+RMB`: spawn a bot
* `Alt+F4`: End trade session.

-----------------------------------------------------

Development
-----------

It's Haskell time!

* https://github.com/jonascarpay/apecs
  * https://github.com/jonascarpay/apecs/tree/master/examples
* https://hackage.haskell.org/package/apecs-gloss
  * https://hackage.haskell.org/package/apecs

### Ubuntu

```shell
sudo apt install freeglut3-dev libsdl2-mixer-dev libsdl2-image-dev
```

### Nix

#### VSCode(ium)
1. Install 'Nix environment selector' plugin for Codium (will be recommended)
2. Select `shell.nix` as your environment.

After several minutes of downloading, HIE and Stack
will be installed and working.

#### Emacs
1. Install [direnv](https://direnv.net/)
2. Run `direnv allow .` in repo directory
3. Install `direnv` in Emacs
4. Follow [these instructions](https://github.com/haskell/haskell-ide-engine#using-hie-with-emacs)

Running
-------

### Windows binary

Unzip and run.

### Linux AppImage

Just run the AppImage. But you

```shell
./SpaceMar!-bf269f8-x86_64.AppImage
```

### From source

```shell
stack run
```

To find where the compiled binary is:

```shell
stack build
stack exec -- which spacemar
```
