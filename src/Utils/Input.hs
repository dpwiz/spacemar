module Utils.Input
  ( keyEvent
  , shiftClick

  -- * Gloss re-exports
  , Event(..)
  , Key(..)
  , MouseButton(..)
  , KeyState(..)
  , Modifiers(..)
  , SpecialKey(..)
  ) where

import Graphics.Gloss.Interface.IO.Interact

keyEvent :: Key -> Event -> Maybe KeyState
keyEvent k = \case
  EventKey ek state _mods _pos
    | k == ek   -> Just state
    | otherwise -> Nothing
  _ ->
    Nothing

shiftClick :: MouseButton -> KeyState -> Event -> Maybe (Float, Float)
shiftClick mb ss = \case
  EventKey (MouseButton emb) Down Modifiers{shift} pos
    | mb == emb && shift == ss -> Just pos
    | otherwise -> Nothing
  _ ->
    Nothing
