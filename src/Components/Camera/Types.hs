{-# LANGUAGE TemplateHaskell #-}

module Components.Camera.Types
  ( Tracker(..)
  , trackerTimer
  , trackerParent
  , trackerVirtual
  , trackerWeight

  , dummy

  , Camera(..)
  ) where

import Apecs (Component(..), Entity, Map)
import Apecs.Gloss (Camera(..))
import Control.Lens.TH (makeLenses)

data Tracker = Tracker
  { _trackerTimer   :: Float        -- ^ Animation timer
  , _trackerParent  :: Maybe Entity -- ^ For parent to collect its trackers
  , _trackerVirtual :: Bool         -- ^ Camera tracks its own position
  , _trackerWeight  :: Float        -- ^ Centroid calculation weight
  }
  deriving (Show)

instance Component Tracker where
  type Storage Tracker = Map Tracker

makeLenses ''Tracker

dummy :: Tracker
dummy = Tracker
  { _trackerTimer   = 0
  , _trackerParent  = Nothing
  , _trackerVirtual = False
  , _trackerWeight  = 1.0
  }
