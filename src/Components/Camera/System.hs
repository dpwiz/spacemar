{-# LANGUAGE AllowAmbiguousTypes #-}

module Components.Camera.System
  ( attach
  , attach_
  , attachVirtual
  , attachVirtual_

  , detach
  , detachVirtual

  , trackWith
  , centroidWith

  , stopTracking

  , tick
  , worldToWindow
  ) where

import Apecs
import Control.Lens
import Control.Monad (void, when)
import Linear (V2(..), lerp, _x, _y, (^*))

import Components.Camera.Types

attach_ :: Set w IO Tracker => Entity -> SystemT w IO ()
attach_ = attach 0 1.0

attach :: Set w IO Tracker => Float -> Float -> Entity -> SystemT w IO ()
attach timer weight parent =
  parent $= Tracker
    { _trackerTimer   = timer
    , _trackerWeight  = weight
    , _trackerParent  = Just parent
    , _trackerVirtual = False
    }

attachVirtual
  :: (Set w IO c, Set w IO Tracker, Has w IO EntityCounter)
  => Float -> Float -> c -> Entity -> SystemT w IO Entity
attachVirtual timer weight extras parent =
  newEntity
    ( Tracker
        { _trackerTimer   = timer
        , _trackerWeight  = weight
        , _trackerParent  = Just parent
        , _trackerVirtual = True
        }
    , extras
    )

attachVirtual_
  :: (Set w IO c, Set w IO Tracker, Has w IO EntityCounter)
  => c -> Entity -> SystemT w IO ()
attachVirtual_ extras = void . attachVirtual 0 1.0 extras

detach :: (Has w IO Tracker) => Entity -> SystemT w IO ()
detach parent =
  cmapM_ $ \(Tracker{..}, e) ->
    when (_trackerParent == Just parent) $
      e $= Not @Tracker

detachVirtual :: (Has w IO Tracker) => Entity -> SystemT w IO ()
detachVirtual parent =
  cmapM_ $ \(Tracker{..}, e) ->
    when (_trackerParent == Just parent && _trackerVirtual) $
      e $= Not @Tracker

trackWith
  :: (Get w IO Camera, Get w IO Tracker, Get w IO posComp)
  => (posComp -> V2 Float) -> Float -> (Int, Int) -> SystemT w IO ()
trackWith getPos bias screen = do
  Camera{..} <- get global
  centroidWith getPos >>= \case
    Nothing ->
      pure ()
    Just centroid -> do
      let offset = lerp bias camOffset centroid

      (tracked, box) <- flip cfold (0, (0,0,0,0)) $ \(num, (minx, miny, maxx, maxy)) (Tracker{}, posComp) ->
        let
          V2 posX posY = getPos posComp
        in
          ( succ num
          , ( min minx posX
            , min miny posY
            , max maxx posX
            , max maxy posY
            )
          )

      let
        fitScale =
          if tracked > (1 :: Int) then
            min 1 $ fitBox screen box
          else
            camScale

      global $= Camera
        { camScale  = fitScale
        , camOffset = offset
        }

fitBox :: (Int, Int) -> (Float, Float, Float, Float) -> Float
fitBox screen box =
  min (halfWidth / deltaX) (halfHeight / deltaY) * 0.9

  where
    (screenWidth, screenHeight) = screen
    halfWidth = fromIntegral screenWidth / 2
    halfHeight = fromIntegral screenHeight / 2

    (minx, miny, maxx, maxy) = box
    deltaX = maxx - minx
    deltaY = maxy - miny

centroidWith
  :: (Get w IO posComp, Get w IO Tracker)
  => (posComp -> V2 Float) -> SystemT w IO (Maybe (V2 Float))
centroidWith getPos = do
  tracked <- flip cfold mempty $ \acc (Tracker{_trackerWeight}, posComp) ->
    (getPos posComp, _trackerWeight) : acc
  case tracked of
    [] ->
      pure Nothing
    some -> do
      let
        (vecs, weights) = unzip some
        total = sum weights
        weightedAvg xs = sum (zipWith (*) weights xs) / total
      pure . Just $
        V2
          (weightedAvg $ map (view _x) vecs)
          (weightedAvg $ map (view _y) vecs)

stopTracking
  :: forall extras w . (Has w IO extras, Destroy w IO extras, Set w IO Tracker)
  => SystemT w IO ()
stopTracking =
  cmapM_ $ \(Tracker{_trackerVirtual}, e) ->
    if _trackerVirtual then
      e $= Not @(Tracker, extras)
    else
      e $= Not @Tracker

tick :: (Set w IO Tracker) => Float -> SystemT w IO ()
tick dt =
  cmap $ \tracker@Tracker{_trackerTimer=timer} ->
    if timer == 0 then
      Left ()
    else
      Right tracker
        {  _trackerTimer = max 0 (timer - dt)
        }

worldToWindow :: Camera -> V2 Float -> V2 Float
worldToWindow Camera{..} xy = (xy - camOffset) ^* camScale
