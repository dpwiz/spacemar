module Draw where

import Data.List (find)
import Graphics.Gloss
import Linear.Affine (distanceA)
import Linear.Metric (normalize)
import Linear.Vector ((^*))
import Text.Printf (printf)

import Components

import qualified Config
import qualified Scene.Gameplay.Types.Ship as Ship
import qualified Utils
import qualified Utils.Trajectory as Trajectory

tractor :: Float -> (Position, Maybe Position) -> Picture
tractor range (Position origin, locked) =
  case locked of
    Nothing ->
      seek
    Just (Position target) ->
      beam origin target
  where
    seek =
      translateV origin $
        color col $
          circle range

    beam (V2 ox oy) (V2 tx ty) =
      color col $
        line [(ox, oy), (tx, ty)]

    col =
      case locked of
        Nothing ->
          blue
        Just _pos ->
          cyan

particle :: (Position, Velocity, Particle) -> Picture
particle (Position p, Velocity v, Particle{..}) =
  translate px py $
    color (withAlpha (_particleTimer) _particleColor) $
      line
        [ (0, 0)
        , (vx, vy)
        ]
  where
    V2 px py = p
    V2 vx vy = v ^* (1/30)

planet :: Bool -> (Planet, Position) -> Picture
planet drawRings (Planet{..}, Position pos) =
  translate gx gy $
    mconcat
      [ halfLanded
      , landing
      , body
      , if drawRings then rings else mempty
      ]

  where
    V2 gx gy = pos

    landingR =
      case _planetRings of
        g8 : _outerRs ->
          g8
        [] ->
          error "no rings plotted"

    halfLanded =
      color (withAlpha 0.125 azure) $
        circleSolid (_planetRadius * 0.5 + landingR * 0.5)

    landing =
      color (withAlpha 0.25 azure) $
        circleSolid landingR

    body = mconcat
      [ color (dim _planetColor) $
          circleSolid _planetRadius
      , color _planetColor $
          arcSolid (-90) 90 _planetRadius
      , color _planetColor $
          translate (negate _planetRadius) 0 $
            rotate 180 $
              arcSolid (-135) 135 (sqrt 2 * _planetRadius)
      ]

    ringColors =
      [ withAlpha (12/16) cyan
      , withAlpha (10/16) white
      , withAlpha (8/16) yellow
      , withAlpha (6/16) white
      , withAlpha (4/16) white
      ]

    rings = mconcat $ do
      (r, c) <- zip _planetRings ringColors
      pure $
        color c $
          circle r

warpIn :: (WarpIn, Position) -> Picture
warpIn (WarpIn{..}, Position p) =
  translateV p $
    rotate (90 * alpha) $
      color (withAlpha alpha col) $
        lineLoop
          [ (len, 0)
          , (0, negate len)
          , (negate len, 0)
          , (0, len)
          ]
  where
    len = _warpInTimer * Config.warpInRadius
    alpha = (1.0 - _warpInTimer / Config.warpInTimer) ** 2
    col = mixColors alpha (1 - alpha) white red

ship :: Bool -> (Ship, Direction, Thrust, Stock, (Fuel, Score), Controls) -> Picture
ship showScore (Ship{..}, Direction d, Thrust t, shipStock, fs, controls) =
  let
    (Fuel f, Score{..}) = fs

    theRust =
      if t > 0 then
        color red $
          line [ (0, 0), (tx, ty) ]
      else
        mempty
      where
        V2 tx ty = negate $ Utils.facing d ^* 15

    theBody =
      rotate d $ mconcat
        [ color col (hull _shipHull)
        , translate (Ship.hullCargoOffset _shipHull) 0 $
            stock shipStock
        ]

    col = case controls of
      Orbit ->
        red
      TakeOff{} ->
        yellow
      Ground ->
        green
      Landing ->
        cyan

    fuel =
      translate 0 15 $ scale 0.14 0.12 $
        color red $
          text $
            show @Integer (round f)

    score =
      translate 0 (- 30) $ scale 0.14 0.12 $
          color (greyN 0.33) $ mconcat
            [                      text $ printf "T: %.1f" _scoreTrade
            , translate 0 (-150) $ text $ printf "F: %.1f" _scoreFuel
            ]
  in
    mconcat
      [ theRust <> theBody
      , if showScore then
          fuel <> score
        else
          mempty
      ]

hull :: Ship.Hull -> Picture
hull = \case
  Ship.Courier ->
    polygon
      [ (20, 0)
      , (0, -5)
      , (-7.5, 0)
      , (0, 5)
      ]

  Ship.Lifter -> mconcat
    [ polygon
        [ (25, -5)
        , (25, 5)
        , (5, 9)
        , (5, -9)
        ]
    , polygon
        [ (5, 9)
        , (5, -9)
        , (-10, 5)
        ]
    , polygon
        [ (5, -9)
        , (5, 9)
        , (-10, -5)
        ]
    , color (withAlpha 0.2 black) $
        translate (off / 2 - 4) 0 $
          rectangleSolid off 10
    ]
    where
      off = Ship.hullCargoOffset Ship.Lifter + 4

stock :: Stock -> Picture
stock (Stock cs) = mconcat $ do
  (offset, comm) <- zip [-2, -4 ..] cs
  let
    marks = case comm of
      Literallium -> red
      Stuff       -> yellow
      Awesomium   -> cyan

  pure $ color marks $ line
    [ (offset, -5)
    , (offset, 5)
    ]

market :: (Position, Commodity, PriceHistory) -> Picture
market (Position p, c, PriceHistory ph) =
    let
      V2 ox oy = p
      V2 tx ty = p + V2 100 100

      extLine =
        color green $
          line [(ox, oy), (tx, ty), (tx + 100, ty)]

      title =
        translate (tx + 10) (ty + 10) $
          scale 0.2 0.125 $
            color green $
              text $
                show @Commodity c
      chartBg =
        color (withAlpha 0.15 green) (polygon chartPoints)

      chartPoints =
        [ (tx, ty)
        , (tx + 100, ty)
        , (tx + 100, ty - 100)
        , (tx, ty - 100)
        ]

      chartLine =
        translate tx (ty - 100) $
          -- scale 10 10 $
            color green $
              line $ zip [0..] ph

    in
      mconcat
        [ chartBg
        , chartLine
        , extLine
        , title
        ]

navLine :: Float -> V2 Float -> V2 Float -> Picture
navLine g gp mp =
  mconcat
    [ theLine
    , thePull
    ]
  where
    V2 gx gy = gp
    V2 mx my = mp

    theLine =
      color (makeColor 1 1 1 0.3) $
        line [(gx, gy), (mx, my)]

    dist = distanceA gp mp
    force = min dist $ Trajectory.pull g gp mp * 10
    V2 vx vy = mp + normalize (gp - mp) ^* force

    thePull =
      color green $
        line [(mx, my), (vx, vy)]

trace :: Trace -> Picture
trace (Trace path) =
  mconcat $ do
    (i, (V2 cx cy, V2 prx pry)) <- zip [0.0 ..] segments
    pure $
      color (withAlpha ((len - i) / len) yellow) $
        line [(cx, cy), (prx, pry)]
  where
    segments = zip path (drop 1 path)
    len = fromIntegral (length path)

trajectory :: Int -> Trajectory -> Picture
trajectory _limit (Trajectory []) = mempty
trajectory limit (Trajectory segments') =
  mconcat
    [ fall
    , apoMark
    , periMark
    , landingMark
    ]

  where
    Trajectory.Segment{sIx=startIx, sPos=startPos, sNorm=startNorm} = head segments'

    path = take limit $ Utils.reducedPath isBoring Trajectory.sPos segments'

    isBoring s =
      case Trajectory.sAnn s of
        Trajectory.Fall | Trajectory.sIx s < 2 ->
          Just False
        Trajectory.Fall ->
          Nothing
        _ ->
          Just False

    fall = mconcat $ do
      (ix, (_seg, V2 ax ay, V2 bx by)) <- zip [0..] unLooped

      let alpha = sqrt $ (len - ix) / len

      pure $
        color (withAlpha alpha cyan) $
          line [(ax, ay), (bx, by)]
      where
        len = fromIntegral $ length unLooped

        unLooped = takeWhile (not . isLooped) path

        isLooped (Trajectory.Segment{..}, _a, _b)
          | sIx - startIx < 10 = False
          | abs (sNorm - startNorm) > startNorm = False
          | otherwise = distanceA sPos startPos < 4

    apoMark = case find isApsis path of
      Nothing ->
        mempty
      Just (Trajectory.Segment{sPos=V2 mx my}, _a, _b) ->
        translate mx my $
          color magenta $
            circle 3.0
      where
        isApsis (Trajectory.Segment{sAnn}, _a, _b) =
          sAnn == Trajectory.Apoapsis

    periMark = case find isApsis path of
      Nothing ->
        mempty
      Just (Trajectory.Segment{sPos=V2 mx my}, _a, _b) ->
        translate mx my $
          color green $
            circle 3.0
      where
        isApsis (Trajectory.Segment{sAnn}, _a, _b) =
          sAnn == Trajectory.Periapsis

    landingMark = case landing of
      Nothing ->
        mempty
      Just (V2 mx my) ->
        translate mx my $
          color cyan $
            circle 3.0
      where
        landing =
          case take 1 (reverse path) of
            [] ->
              Nothing
            (Trajectory.Segment{sPos, sAnn}, _a, _b) : _prev ->
              case sAnn of
                Trajectory.Landing ->
                  Just sPos
                _ ->
                  Nothing

translateV :: V2 Float -> Picture -> Picture
translateV (V2 px py) = translate px py
