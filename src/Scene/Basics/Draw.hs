module Scene.Basics.Draw where

import Apecs (get, global)
import Graphics.Gloss

import Components

-- import qualified Draw
import qualified Draw.UI as UI
import qualified Scene.Gameplay.Draw as Gameplay
import qualified Components.Fade.Draw as Fade

draw :: SystemW Picture
draw = Gameplay.draw

drawUI :: Screen -> SystemW Picture
drawUI screen = do
  Toggles{..} <- get global

  fades <- Fade.drawUI screen

  pure $ mconcat
    [ if _toggleGrid then
        UI.uiGrid _toggleGridPitch screen
      else
        mempty
    , fades
    ]
