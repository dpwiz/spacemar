module Scene.Basics.Transition
  ( enterFrom
  , leaveFor
  ) where

import Apecs (cmap, cmapM_, get, global, ($=))
import GHC.Float (double2Float)

import Components
import Utils.Debug (traceM)
import Utils.NSA (NoneSelfAll(..))

import qualified Components.Camera.System as Camera
import qualified Components.Delayed.System as Delayed
import qualified Components.Fade.System as Fade
import qualified Components.Input.System as Input
import qualified Config
import qualified Scene.Basics.Controls.Types as Controls
import qualified Scene.Gameplay.Types.Ship as Ship
import qualified System.Actions as Actions

enterFrom :: Scene -> SystemW ()
enterFrom = \case
  scene -> do
    traceM $ "Basics: entering from " <> show scene

    global $= Basics
    global $= (mempty :: Camera)

    global $= mempty
      { _toggleNavs    = All
      , _toggleMarkets = Self
      , _toggleScore   = None
      , _toggleTrajecs = All
      , _toggleRings   = True
      , _toggleGrid    = False
      }

    Input.attach_ global Controls.UI

    global $= BotLimit 0

    planet <- Actions.planetSpawn
      (V2 0 0)
      0
      (double2Float Config.planetSpawnGravMean)
      (Just Stuff)
    Camera.attach_ planet

    get planet >>= \case
      Planet{..} -> do
        let worldXY = (V2 _planetRadius 0)
        Actions.shipWarpIn Player Ship.Courier worldXY $ \new -> do
          new $= (Player, Stock [])
          Camera.attach_ new
          Input.attach_ new Controls.PlayerMouse

        Actions.shipWarpIn Player Ship.Courier worldXY $ \new -> do
          new $= (Player, Stock [])
          Camera.attach_ new
          Input.attach_ new Controls.PlayerKeys1

        Actions.shipWarpIn Player Ship.Courier worldXY $ \new -> do
          new $= (Player, Stock [])
          Camera.attach_ new
          Input.attach_ new Controls.PlayerKeys2

    Fade.fadeIn 1.0

leaveFor :: Scene -> SystemW ()
leaveFor = \case
  scene -> do
    traceM $ "Basics: leaving for " <> show scene

    Delayed.cancelAll
    Input.destroyAll

    cmap $ \WarpIn{} ->
      Not @(WarpIn, Position)

    cmap $ \Particle{} ->
      Not @(Particle, Position, Velocity)

    cmap $ \IntroMenu{} ->
      Not @IntroMenu

    cmapM_ $ \(Ship{}, e) ->
      Actions.shipDestroy e

    cmapM_ $ \(Crate, e) ->
      Actions.crateDestroy e

    cmapM_ $ \(Planet{}, e) ->
      Actions.planetDestroy e

    traceM $ "Basics: left for " <> show scene
