module Scene.Basics.Tick where

import System.Handlers (TickHandler)

import qualified Scene.Gameplay.Tick as Gameplay

-- | Run the thing.
tick :: TickHandler
tick = Gameplay.tick
