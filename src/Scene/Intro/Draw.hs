module Scene.Intro.Draw where

import Apecs (get, global)
import Apecs.Gloss (foldDraw)
import Graphics.Gloss

import Components

import qualified Draw
import qualified Draw.UI as UI
import qualified Scene.Gameplay.Draw as Gameplay
import qualified Components.Fade.Draw as Fade

draw :: SystemW Picture
draw = Gameplay.draw

drawUI :: Screen -> SystemW Picture
drawUI screen = do
  Toggles{..} <- get global

  menu <- foldDraw drawMenu
  fades <- Fade.drawUI screen

  pure $ mconcat
    [ if _toggleGrid then
        UI.uiGrid _toggleGridPitch screen
      else
        mempty
    , translate marginLeft marginTop $ mconcat
        [ title
        , subtitle
        , translate 0 (-150) menu
        ]
    , fades
    ]
  where
    marginLeft =
      negate $
        fromIntegral (_screenWidth screen) * 0.333

    marginTop =
      fromIntegral (_screenHeight screen) * 0.25

    title =
      color white $
        text "SpaceMar!"

    subtitle =
      translate 150 (-25) $
        scale 0.2 0.2 $
          color cyan $
            text "Make trade not war"

drawMenu :: IntroMenu -> Picture
drawMenu IntroMenu{..} = items
  where
    items = mconcat $ do
      (ix, item) <- zip [0..] [minBound .. maxBound]
      let
        marker =
          if item == _introMenuSelected then
            translate (-17) (10) $
              rotate cursorDeg $
                translate 8 0 cursor
          else
            mempty
      pure .
        translate 0 (-50 * ix) $
          mappend marker .
            color yellow $
              scale 0.2 0.2 $
                text $ case item of
                  StartGame   -> "Apply for promotion"
                  StartLounge -> "Observation lounge"
                  StartBasics -> "Orbiting basics"
                  QuitGame    -> "Quit"

    cursor = Draw.stock $ Stock
      [ Literallium, Literallium
      , Stuff, Stuff
      , Awesomium, Awesomium
      ]

    cursorDeg =
      fromIntegral @Int $
        floor (_introMenuTimer * 90) `rem` 360
