module Scene.Intro.Tick where

import Apecs
import Control.Lens ((+~))
import Linear (lerp)

import Components
import System.Handlers (TickHandler)

import qualified Components.Delayed.System as Delayed
import qualified Components.Fade.System as Fade
import qualified Scene.Gameplay.Tick as Gameplay
import qualified Scene.Intro.Transition as Intro
import qualified Scene.Lounge.Transition as Lounge
import qualified Utils

-- | Just run the usual in background.
tick :: TickHandler
tick dt = do
  Gameplay.tickBg dt

  introMenu dt

introMenu :: TickHandler
introMenu dt = cmapM_ $ \IntroMenu{..} -> do
  global $~ introMenuTimer +~ dt

  cmapM_ $ \(Planet{}, Position pos) -> do
    let alpha = Utils.rangeNormSigm (30, 30 + 60*4) _introMenuTimer
    if alpha == 1.0 then
      Fade.fadeOut 0.5 $
        Delayed.once_ 0.5 $ do
          Intro.leaveFor Lounge
          Lounge.enterFrom Intro
    else
      global $= Camera
        { camOffset = lerp alpha pos (V2 0 0)
        , camScale  = 1 / (1 + 2 * alpha)
        }
