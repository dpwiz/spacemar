module Scene.Lounge.Draw where

import Apecs (get, global)
import Apecs.Gloss (foldDraw)
import Graphics.Gloss

import Components
import Utils.Draw (translateV2)

-- import qualified Draw
import qualified Components.Camera.Types as Camera
import qualified Components.Fade.Draw as Fade
import qualified Draw.UI as UI
import qualified Scene.Gameplay.Draw as Gameplay

draw :: SystemW Picture
draw = do
  sim <- Gameplay.draw

  Camera{..} <- get global

  track <- foldDraw $ \(Camera.Tracker{..}, Position pos) ->
    translateV2 pos $
      color (withAlpha _trackerTimer red) $
        circle $ 100 * _trackerWeight / camScale

  pure $ mconcat
    [ sim
    , track
    ]

drawUI :: Screen -> SystemW Picture
drawUI screen = do
  Toggles{..} <- get global

  fades <- Fade.drawUI screen

  pure $ mconcat
    [ if _toggleGrid then
        UI.uiGrid _toggleGridPitch screen
      else
        mempty
    , fades
    ]
