module Scene.Outro.Tick where

import Components

-- | Just run the usual in background.
tick :: Float -> SystemW ()
tick _dt =
  pure ()
