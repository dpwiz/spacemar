module Scene.Outro.Draw where

import Apecs.Gloss (foldDraw)
import Graphics.Gloss
import Text.Printf (printf)

import Components

import qualified Components.Fade.Draw as Fade
import qualified Draw
import qualified Scene.Gameplay.Types.Ship as Ship

draw :: SystemW Picture
draw = pure mempty

drawUI :: Screen -> SystemW Picture
drawUI screen = do
  score <- foldDraw drawScore

  fades <- Fade.drawUI screen
  pure $ mconcat
    [ mempty
    -- , UI.uiGrid 10 screen
    , score
    , fades
    ]

drawScore :: OutroScore -> Picture
drawScore OutroScore{..} =
  mconcat
    [ translate (-100) 0 $
        mappend shipFrame $
          scale 2 2 . rotate deg $ mconcat
            [ color red $ Draw.hull _shipHull
            , translate (Ship.hullCargoOffset _shipHull) 0 $
                Draw.stock stock
            ]
    , scale 0.2 0.2
        score
    ]
  where
    (Ship{_shipHull}, stock, deg) = _outroScorePortrait

    shipFrame =
      color red $
        rectangleWire 100 100

    score =
      translate 0 initialOffset $ mconcat $ do
        (i, (col, msg)) <- zip [0..] scoreMessage
        pure $
          translate 0 (negate $ i * lineSize) $
            color col $ text msg

    emptyLine = (black, "")

    scoreMessage =
      [ ( redgreen _outroScoreTradeEff
        , printf "Trade efficiency: %.1f" _outroScoreTradeEff
        )
      , ( redgreen _outroScoreFuelEff
        , printf "Fuel efficiency: %.1f" _outroScoreFuelEff
        )
      , ( white
        , printf "Operation balance: %.1f" _outroScoreFinalFuel
        )
      , if _outroScoreGotStuff then
          (white, "Stuff acquisition complete")
        else
          (red, "Stuff acquisition failed")
      , emptyLine
      , ( white
        , "Performance rating: " <> take 1 _outroScoreRating
        )
      , ( _outroScoreColor
        , _outroScoreRating
        )
      , (white, "Your new title is: " <> _outroScoreTitle)
      ] ++
      [ (white, "But you are still awesome!")
      | _outroScoreColor == red && _outroScoreGotAwesome
      ]

    lineSize = 250
    initialOffset = fromIntegral (length scoreMessage - 1) * lineSize / 2

    redgreen n = case compare n 0 of
      LT -> red
      EQ -> white
      GT -> green
