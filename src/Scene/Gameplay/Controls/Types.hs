module Scene.Gameplay.Controls.Types where

import Components (Toggles)

data Schema
  = PlayerKeys1
  | PlayerKeys2
  | PlayerMouse
  | UI
  deriving (Eq, Ord, Show, Enum, Bounded)

data ShipActions
  = ThrustStart
  | ThrustStop
  | TurnCw
  | TurnCcw
  | TurnStop
  | TractorStart
  | TractorStop
  | Bankrupt

data OrientActions
  = OrientTurn   (Float, Float)
  | OrientBurn   (Float, Float)
  | OrientUpdate (Float, Float)
  | OrientStop
  | Respawn (Float, Float) -- XXX: dev cheat

data UiAction
  = ExitToMenu
  | PauseResume
  | UIToggle (Toggles -> Toggles)
  | SpawnBot (Float, Float)
