{-# LANGUAGE OverloadedLists #-}
module Scene.Gameplay.Tick
  ( tick
  , tickBg
  ) where

import Apecs (Not(..), cfold, cmap, cmapM, cmapM_, destroy, get, global, newEntity, ($=), ($~))
import Apecs.Gloss (Camera(..))
import Control.Lens ((&), (%~), (+~))
import Control.Monad (void, when)
import Data.Bifunctor (bimap)
import Data.Proxy (Proxy(..))
import GHC.Float (float2Double, double2Float)
import Linear (V2(..), angle, (^*))
import Linear.Affine (qdA)
import Linear.Metric (norm, normalize)
import Text.Printf (printf)

import qualified Apecs.System.Random as Random
import qualified Data.List as List
import qualified Graphics.Gloss as G
import qualified System.Random.MWC.Probability as Probability

import Components
import System.Actions (ClosestPlanet(..), CrateComponents)
import System.Handlers (TickHandler)

import qualified Components.Camera.System as Camera
import qualified Config
import qualified Scene.Gameplay.Controls.Bot as Bot
import qualified Scene.Gameplay.Transition as Gameplay
import qualified Scene.Gameplay.Types.Ship as Ship
import qualified System.Actions as Actions
import qualified Utils
import qualified Utils.Apecs as Apecs
import qualified Utils.Trajectory as Trajectory

tick :: TickHandler
tick = tick_ False

tickBg :: TickHandler
tickBg = tick_ True

tick_ :: Bool -> TickHandler
tick_ background dt = do
  let fg = when (not background)

  Pause paused <- get global
  when (not paused) $ do
    warpIn dt
    moveParticles dt

    controlsTimer dt

    applyOrbits dt
    applyOrient dt
    applyTractor dt
    applyThrust dt
    applyTurn dt
    updateWarp dt

    moveTraced dt
    projectTrajectories background dt

    sellCrate dt
    updateMarkets dt

    payForThrust dt
    updateFuel dt
    updateScore dt

    botSpawn background dt
    botControls background dt

    checkWarp background dt

    fg $ do
      global $~ messageLogLineTimer . Utils.trimming 0.0 1.0 +~ dt

      Actions.cameraTrack
      Camera.tick dt

warpIn :: Float -> SystemW ()
warpIn dt = cmapM_ $ \(WarpIn{..}, Position pos, e) -> do
  let timer = _warpInTimer - dt
  if timer <= 0 then
    destroy e $ Proxy @WarpIn
  else do
    e $= WarpIn
      { _warpInTimer = timer
      }
    spark <- Random.sample $ Probability.bernoulli 0.5
    when spark $
      Actions.spawnParticles (-1) pos

moveParticles :: TickHandler
moveParticles dt =
  cmap $ \(Particle{..}, Position pos, Velocity vel) ->
  let
    pt' = _particleTimer - dt
  in
    if pt' <= 0 then
      Left $ Not @(Position, Velocity, Particle)
    else
      Right
        ( Position $ pos + vel ^* dt
        -- , Velocity vel
        , Particle _particleColor pt'
        )

controlsTimer :: TickHandler
controlsTimer dt = cmap $ \(ControlsTimer t) ->
  ControlsTimer (t + dt)

applyOrbits :: TickHandler
applyOrbits _dt =
  cmapM_ $ \(Mass{}, controls, mship, mbot, Velocity curV, Position sp, ship) -> do
    planets <- Actions.closestPlanets sp

    let
      botControlsChanged new = case mbot of
        Nothing ->
          pure ()
        Just bot ->
          Bot.controlsChanged
            ship
            bot
            planets
            (controls, new)

    case controls of
      Ground ->
        -- XXX: sit tight
        pure ()

      Orbit ->
        -- XXX: free fall
        case planets of
          [] ->
            -- XXX: no planets to pull or to land on
            pure ()

          ClosestPlanet{..} : _rest
            | cpDistance <= head (_planetRings cpPlanet) -> do
                ship $=
                  ( Landing
                  , Thrust 0
                  , Turn 0
                  , Trajectory []
                  , Not @Orient
                  )
                botControlsChanged Landing

          _ -> do
            let
              deltaV = sum $ do
                ClosestPlanet{..} <- planets
                pure $ Trajectory.pullV cpGravity cpPosition sp
            ship $= Velocity (curV + deltaV)

      Landing ->
        -- XXX: tractor beam down
        case planets of
          [] ->
            error "wait, what?"
          ClosestPlanet{..} : _rest -> do
            Direction srcD <- get ship
            let
              unit = normalize (cpPosition - sp)

              fixup = unit ^* (cpDistance - _planetRadius cpPlanet)

              V2 ncos nsin = unit
              sdeg = asin nsin / pi * 180
              cdeg = acos ncos / pi * 180
              dstD =
                if sdeg < 0 then
                  cdeg - 180
                else
                  180 - cdeg

            if cpDistance <= _planetRadius cpPlanet then do
              ship $=
                ( Ground
                , Position $ sp + fixup ^* 1.01
                , Velocity 0
                , Thrust 0
                , Turn 0
                , Direction dstD
                , Trace []
                , Not @Orient
                )
              botControlsChanged Ground
            else do
              let
                descent = curV ^* 0.95 + unit
                newD =
                  if
                    | abs srcD + abs dstD < 180 ->
                        srcD * 0.9 + dstD * 0.1
                    | srcD < 0 && dstD > 0 ->
                        srcD * 0.9 + (dstD - 360) * 0.1
                    | srcD > 0 && dstD < 0 ->
                        srcD * 0.9 + (dstD + 360) * 0.1
                    | otherwise ->
                        srcD * 0.9 + dstD * 0.1
              ship $=
                ( Velocity descent
                , Direction $ Utils.norm180 newD
                )

      TakeOff outside ->
        case planets of
          [] ->
            error "wait, what?"
          ClosestPlanet{..} : _rest -> do
            let
              (landingR, takeoffR) =
                case _planetRings cpPlanet of
                  g8 : _g4 : g2 : _outer ->
                    (g8, g2)
                  _ ->
                    error "assert: third ring is g2"

              -- XXX: Thrust provided per frame
              counterGravity = Trajectory.pull cpGravity cpPosition sp
              extraThrust =
                case mship of
                  Nothing       -> 1.0
                  Just Ship{..} -> Ship.hullThrust _shipHull
              provided = 1 * counterGravity + 0.5 * extraThrust

            if
              | cpDistance > takeoffR -> do
                  -- Left
                  ship $= (Thrust 0, Orbit)
                  botControlsChanged Orbit

              | cpDistance > landingR -> do
                  -- Leaving
                  when (not outside) $
                    ship $= TakeOff True
                  ship $= Thrust provided

              | cpDistance < _planetRadius cpPlanet -> do
                  -- Hard stop
                  ship $= (Thrust 0, Turn 0, Velocity 0, Landing)
                  botControlsChanged Landing

              | outside -> do
                  -- Soft stop
                  ship $= (Thrust 0, Turn 0, Landing)
                  botControlsChanged Landing

              | otherwise ->
                  -- Initial lift
                  ship $= Thrust provided

applyOrient :: TickHandler
applyOrient dt =
  cmapM_ $ \(Orient{..}, Ship{..}, controls, Position pos, Velocity vel, Direction srcD, ship) -> do
    ship $~ orientPoint +~ vel ^* dt
    case controls of
      Landing ->
        ship $= TakeOff False
      Ground ->
        ship $= TakeOff False
      _ -> do
        let
          -- XXX: this is too contrived/general for this task
          deltaD  = srcD - Utils.orient _orientPoint pos srcD 0.0
          turn   = Ship.hullTurn _shipHull * 36
          dir    = if deltaD > 0 then turn else negate turn
        ship $= Turn dir

        -- XXX: limit thrust to prevent unwanted orbit changes
        when (controls == Orbit) $ do
          let thrustQ = 1 - (180 - abs deltaD) / 90
          if _orientBurn && thrustQ > 0 then
            ship $= Thrust (Ship.hullThrust _shipHull * thrustQ)
          else
            ship $= Thrust 0

applyTractor :: TickHandler
applyTractor dt = cmapM $ \(t, Position originPos, origin, ship) -> do
  let
    timer'   = _tractorTimer t + dt
    range    = Config.tractorRange * Utils.rangeNorm (0, 3) timer'
    tractor' = t
      { _tractorTimer = timer'
      , _tractorRange = range
      }

    range2   = range * range

  spark <- Random.sample $ Probability.bernoulli (float2Double dt)
  when spark $
    Actions.spawnParticles (-1) originPos
  get ship >>= \case
    Nothing ->
      pure ()
    Just (Fuel f) ->
      ship $= Fuel (f - dt)

  case _tractorLock t of
    Just e ->
      get e >>= \case
        Just (Orbit, Position locked, Velocity vel, Mass mass) -> do
          spark2 <- Random.sample $ Probability.bernoulli (float2Double dt)
          when spark2 $
            Actions.spawnParticles 1 locked

          pull tractor' range2 (origin, originPos) (e, locked, vel, mass)
        _ ->
          pure Nothing
    Nothing ->
      seek tractor' range2 originPos
  where
    pull tractor' range2 (origin, originPos) (locked, lockedPos, lockedVel, lockedMass) = do
      let
        dist2 = qdA originPos lockedPos
      if
        | dist2 < 100 -> do
            Stock stock <- get locked
            origin $= Stock stock
            Actions.crateDestroy locked
            pure Nothing

        | dist2 > range2 ->
          pure Nothing

        | otherwise -> do
            let vec = originPos - lockedPos
            let force = 10 / dist2 / lockedMass / dt
            locked $= Velocity (lockedVel + vec ^* force)

            pure $ Just tractor'

    seek tractor range2 originPos = do
      crates <- Apecs.getAllWith $ \(Position pos, Crate{}, e) ->
        let
          dist2 = qdA originPos pos
          inRange = dist2 <= range2
        in
          (inRange, (dist2, e))

      lockOn <- case List.sort . map snd $ filter fst crates of
        [] ->
          pure Nothing
        (_dist2, closest) : _rest -> do
          pure $ Just closest

      -- BUG: for some reason this can execute quite a few times per second.
      pure $ Just tractor
        { _tractorLock  = lockOn
        }

applyThrust :: TickHandler
applyThrust _dt =
  cmap $ \(Thrust force, Mass mass, Direction d, Velocity v) ->
    Velocity $ v + Utils.facing d ^* (force / mass)

payForThrust :: TickHandler
payForThrust _dt =
  cmap $ \(controls, Thrust t, Fuel f) -> do
    let cost = t * 0.1
    case controls of
      Orbit ->
        Fuel $ f - cost
      TakeOff{} ->
        -- XXX: bulk discounts! -50%
        -- NOTE: makes takeoff cheaper for lifters than couriers
        Fuel $ f - cost * 0.5
      _ ->
        Fuel f

updateFuel :: TickHandler
updateFuel dt =
  cmap $ \(Fuel f, mstock, c) ->
    if f > 0 then
      -- XXX: dividends
      Fuel $ f + f * Config.fuelInterest * dt
    else
      if c == Ground then
        -- XXX: dotations
        let
          penalty = case mstock of
            Nothing ->
              0
            Just (Stock stock) ->
              length stock
        in
          Fuel $ f + 1 / (1 + fromIntegral penalty)
      else
        Fuel f

updateScore :: TickHandler
updateScore dt =
  cmap $ \(s, Velocity v, Fuel f) ->
    s & scoreFuel +~ (f * 0.01 * dt)
      & scoreVelocity %~ max (log $ norm v)

applyTurn :: TickHandler
applyTurn dt =
  cmap $ \(Turn t, Direction d, active) ->
    let
      resist =
        case active of
          Nothing ->
            1.0
          Just (Mass m) ->
            m * 0.1
    in
      Direction . Utils.norm180 $ d + t * dt / resist

updateWarp :: TickHandler
updateWarp _dt =
  cmap $ \(WarpSpeed old, Velocity vel) ->
    let
      new =
        Utils.rangeNormSigm
          (Config.warpSpeed, Config.warpJump)
          (log (norm vel))
    in
      if old /= new then
        Right $ WarpSpeed new
      else
        Left ()

moveTraced :: TickHandler
moveTraced dt =
  cmap $ \(Trace path, Velocity v, Position p) ->
    let
      pos = p + v ^* dt
      trace =
        case path of
          [] ->
            [pos]
          prev : _rest ->
            if qdA pos prev >= 16.0 then
              pos : take 150 path
            else
              path
    in
      (Position pos, Trace trace)

projectTrajectories :: Bool -> TickHandler
projectTrajectories _background dt =
  cmapM $ \(Trajectory segments', controls, spv, Thrust t) ->
    case controls of
      (Orbit, mwarp) ->
        case (inWarp mwarp, t, segments') of
          (True, _, _ : _) ->
            pure $ Trajectory []
          (False, 0, _seg : next) ->
            -- XXX: just drop one tick out of the stored stream
            pure $ Trajectory next
          _ -> do
            -- XXX: update trajectory
            planets <- Apecs.getAll
            let
              (Position sp, Velocity v) = spv
              bodies = map (bimap unGravity unPosition) planets
              newSegments = Trajectory.project dt bodies (sp, v)
            pure $ Trajectory newSegments
      _ ->
        pure $ Trajectory []
  where
    inWarp = \case
      Nothing            -> False
      Just (WarpSpeed n) -> n /= 0

sellCrate :: TickHandler
sellCrate _dt =
  cmapM_ $ \(Crate, Stock stock, controls, e) ->
    case stock of
      [] ->
        destroy e $ Proxy @(CrateComponents, Bot, Player)
      (sold : stockLeft) -> do
        case controls of
          Ground -> do
            e $= Stock stockLeft
            get e >>= \case
              Nothing ->
                pure ()
              Just Player ->
                cmapM_ $ \(Ship{}, Player, Fuel f, fuelTo) ->
                  Actions.withMarket e $ \_planet comm (PriceHistory ph) -> do
                    let
                      price = case compare sold comm of
                        LT -> Config.priceMean sold * 1.25 -- XXX: a fair offer
                        EQ -> head ph                      -- XXX: that's the price
                        GT -> Config.priceMean sold * 1.5  -- XXX: a luxury here

                    fuelTo $= Fuel (f + price)
                    let
                      score = price / Config.priceMean sold - 1
                      col = if score < 0 then G.red else G.green
                    fuelTo $~ scoreTrade +~ score
                    Actions.logMessage col $
                      printf "crate: %s sold at %.2f" (show sold) price
          _ ->
            pure ()

updateMarkets :: TickHandler
updateMarkets _dt =
  cmapM_ $ \(c, PriceHistory ph, e) -> do
    case ph of
      [] ->
        error "oops, empty history"
      latest : _tail -> do
        price <- Random.sample $ do
          spot <- Probability.normal
            (realToFrac $ Config.priceMean c)
            (Config.priceSD c) -- (max step $ latest - step, latest + step)
          pure $ latest * 0.9 + realToFrac (spot * 0.1)

        e $= PriceHistory (price : take 100 ph)

botControls :: Bool -> TickHandler
botControls background dt = cmapM_ (Bot.tick background dt)

botSpawn :: Bool -> TickHandler
botSpawn _bg dt = do
  botsActive <- flip cfold 0 $ \acc (Bot{}, _ :: Either WarpIn Ship) -> acc + 1
  BotLimit botLimit <- get global

  when (botsActive < botLimit) $ do
    dice <- Random.sample . Probability.bernoulli $
      realToFrac dt / Config.botsSpawnRate

    when dice $ do
      Actions.randomRingPos 2 Config.botsSpawnSD >>= \case
        Nothing ->
          pure ()
        Just pos ->
          Actions.botSpawnShip pos

checkWarp :: Bool -> TickHandler
checkWarp _bg dt =
  cmapM_ $ \(WarpSpeed warp, e) ->
    when (warp > 0) $ do
      effects warp e
      actions warp e
  where
    effects warp e = do
      (Position pos, Velocity vel) <- get e

      dice <- Random.sample . Probability.bernoulli $
        float2Double (60 * dt * warp * (2 ** warp))

      when dice $ do
        components <- Random.sample $ do
          let velRad = Utils.unangle vel
          let distSpread = float2Double $ 8 ** exp warp

          rad  <- Probability.normal   0 (pi/2)
          dist <- Probability.normal   distSpread distSpread
          vel' <- Probability.uniformR (0, warp / 2)
          pure
            ( Position $ pos + angle (velRad + double2Float rad) ^* double2Float dist
            , Velocity $ vel - vel ^* (0.05 + vel')
            , Particle (G.makeColor (0.5 - vel') 1 1 vel') 1.0
            )
        void $ newEntity components

    actions warp e =
      get e >>= \case
        Nothing ->
          pure ()
        Just (Player, Ship{}) -> do
          global $~ \cam -> cam
            { camScale = 1 + warp
            }

          when (warp == 1.0) $
            Gameplay.leaveFor Outro
