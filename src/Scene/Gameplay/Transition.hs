module Scene.Gameplay.Transition
  ( enterFrom
  , leaveFor
  ) where

import Components

import Apecs (cmap, cmapM_, global, ($=))
import Control.Monad (when)

import qualified Graphics.Gloss as G

import Utils.Debug (traceM)

import qualified Components.Delayed.System as Delayed
import qualified Components.Fade.System as Fade
import qualified Components.Input.System as Input
import qualified Config
import qualified Scene.Gameplay.Controls.Types as Controls
import qualified Scene.Intro.Transition as Intro
import qualified Scene.Lounge.Transition as Lounge
import qualified Scene.Outro.Transition as Outro
import qualified System.Actions as Actions
import qualified Utils

enterFrom :: Scene -> SystemW ()
enterFrom scene = do
  traceM $ "Gameplay: entering from " <> show scene

  global $= Gameplay
  global $= mempty @(Camera, Toggles)
  global $= BotLimit Config.botsMax

  welcomeMessages

  Fade.fadeIn 1.0

  Input.attach_ global Controls.UI

  Actions.planetSpawnInitial
  Actions.playerSpawnRandom

  traceM $ "Gameplay: entered from " <> show scene

leaveFor :: Scene -> SystemW ()
leaveFor scene =
  Fade.fadeOut 1.0 $ do
    traceM $ "Gameplay: leaving for " <> show scene
    Input.destroyAll

    when (scene == Outro) setOutroScore

    -- XXX: give delayeds a chance for cleanup (?)
    Delayed.once_ 1.0 $ do
      when (scene /= Lounge) $ do
        teardown
        Delayed.cancelAll

      traceM $ "Gameplay: left for " <> show scene
      case scene of
        Loading ->
          Utils.exit
        Outro ->
          Outro.enterFrom Gameplay
        Lounge ->
          Lounge.enterFrom Gameplay
        Intro ->
          Intro.enterFrom Gameplay
        Gameplay ->
          error "Go bankrupt instead."
        Basics ->
          error "Go via Menu instead."

setOutroScore :: SystemW ()
setOutroScore = do
  cmapM_ $ \(Player, ship, Direction dir, Fuel fuel, Score{..}, stock) -> do
    let
      Stock goods = stock
      gotStuff    = Stuff `elem` goods
      gotAwesome  = Awesomium `elem` goods

      posTrade = _scoreTrade > 0
      posFuel  = _scoreFuel > 0

      cTitle =
        cTitles !! (abs (floor _scoreFuel) `rem` length cTitles)

      cTitles =
        [ "Shinies"
        , "Space"
        , "Sparkling"
        , "Starsales"
        , "Stellar"
        ]

    let
      (col, rating, title) =
        if not gotStuff then
          if posTrade && posFuel then
            (G.red, "Demoted", "Middle Market Manager")
          else
            (G.red, "Fired", "Unemployed")
        else
          if posTrade && posFuel then
            if gotAwesome then
              if fuel > 0 then
                (G.cyan, "Fucking Awesome", "Whatever you'd like it")
              else
                (G.cyan, "Awesome", "Chief Awesome Officer")
            else
              (G.white, "Best in class", "Chief Executive Officer")
          else
            if posTrade || posFuel then
              (G.white, "Competent", "Chief " <> cTitle <> " Officer")
            else
              (G.white, "Compliant", "Chief Stuff Officer")

    let
      score = OutroScore
        { _outroScoreFinalFuel  = fuel
        , _outroScoreTradeEff   = _scoreTrade
        , _outroScoreFuelEff    = _scoreFuel
        , _outroScoreGotStuff   = gotStuff
        , _outroScoreGotAwesome = gotAwesome
        , _outroScoreRating     = rating
        , _outroScoreTitle      = title
        , _outroScoreColor      = col
        , _outroScorePortrait   = (ship, stock, dir)
        }

    global $= score

teardown :: SystemW ()
teardown = do
  cmap $ \WarpIn{} ->
    Not @(WarpIn, Position)

  cmap $ \Particle{} ->
    Not @(Particle, Position, Velocity)

  cmap $ \IntroMenu{} ->
    Not @IntroMenu

  cmapM_ $ \(Planet{}, pe) ->
    Actions.planetDestroy pe

  cmapM_ $ \(Ship{}, e) ->
    Actions.shipDestroy e

  cmapM_ $ \(Crate, e) ->
    Actions.crateDestroy e

welcomeMessages :: SystemW ()
welcomeMessages = do
  global $= (mempty :: MessageLog)
  Actions.logMessage G.white "SpaceMar! session initiated."
  Delayed.once_ 0.5 $ do
    Actions.logMessage G.white "Promotion review in progress."
    Delayed.once_ 0.5 $
      Actions.logMessage G.white "Primary objective: Acquire Stuff sample."
