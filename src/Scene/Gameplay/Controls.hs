module Scene.Gameplay.Controls where

import Apecs (Entity, exists, get, global, ($=), ($~))
import Apecs.Gloss (Event, windowToWorld)
import Control.Lens ((.~), (%~))
import Control.Monad (void, when)

import Components
import Scene.Gameplay.Controls.Types

import Components.Input.Schema
import Components.Input.Modifiers (modShift, (+>))
import Scene.Gameplay.Transition (leaveFor)
import Utils.Input (MouseButton(..), SpecialKey(..))
import Utils.NSA (nsaCycle)

import qualified Components.Input.System as Input
import qualified Scene.Gameplay.Types.Ship as Ship
import qualified System.Actions as Actions

input :: Event -> SystemW ()
input = Input.handle
  [ playerKeys1 True
  , playerKeys2 True
  , playerMouse True
  , ui
  ]

-- * Ship controls / keyboard

playerKeys1 :: Bool -> InputSchema
playerKeys1 allowRespawn = schema PlayerKeys1 (shipActions allowRespawn) playerKeys1Bindings

playerKeys2 :: Bool -> InputSchema
playerKeys2 allowRespawn = schema PlayerKeys2 (shipActions allowRespawn) playerKeys2Bindings

shipActions :: Bool -> Entity -> ShipActions -> SystemW ()
shipActions allowRespawn ship action = do
  (Ship{..}, controls) <- get ship
  let
    thrust = Ship.hullThrust _shipHull
    turn   = Ship.hullTurn   _shipHull * 36

  case action of
    ThrustStart ->
      case controls of
        Orbit ->
          ship $= Thrust thrust
        Landing ->
          ship $= TakeOff False
        Ground ->
          ship $= TakeOff False
        _ ->
          pure ()

    ThrustStop ->
      case controls of
        Orbit ->
          ship $= Thrust 0
        _ ->
          pure ()

    TurnCcw ->
      case controls of
        Orbit ->
          ship $= Turn (negate turn)
        TakeOff{} ->
          ship $= Turn (negate turn)
        Ground ->
          Actions.placeOrder ship (-1)
        _ ->
          pure ()

    TurnCw ->
      case controls of
        Orbit ->
          ship $= Turn turn
        TakeOff{} ->
          ship $= Turn turn
        Ground ->
          Actions.placeOrder ship 1
        _ ->
          pure ()

    TurnStop ->
      case controls of
        Orbit ->
          ship $= Turn 0
        TakeOff{} ->
          ship $= Turn 0
        _ ->
          pure ()

    TractorStart ->
      get ship >>= \case
        (Just Tractor{}, _stock) ->
          -- XXX: a clash in controls?
          pure ()

        (Nothing, Stock []) -> do
          ship $= Tractor
            { _tractorLock  = Nothing
            , _tractorRange = 0
            , _tractorTimer = 0
            }

        (Nothing, Stock _some) ->
          void $ Actions.dumpStock ship

    TractorStop ->
      ship $= Not @Tractor

    Bankrupt ->
      when allowRespawn $ do
        Actions.playerBankrupt ship
        Actions.playerSpawnRandom

playerKeys1Bindings :: Bindings ShipActions
playerKeys1Bindings =
  [ keyToggle 'w'    ThrustStart  ThrustStop
  , keyToggle 'a'    TurnCcw      TurnStop
  , keyToggle 'd'    TurnCw       TurnStop
  , keyToggle '`'    TractorStart TractorStop
  , keyDown   KeyEsc Bankrupt
  ]

playerKeys2Bindings :: Bindings ShipActions
playerKeys2Bindings =
  [ keyToggle KeyUp        ThrustStart  ThrustStop
  , keyToggle KeyLeft      TurnCcw      TurnStop
  , keyToggle KeyRight     TurnCw       TurnStop
  , keyToggle KeyCtrlR     TractorStart TractorStop
  , keyUp     (modCtrl +> KeyCtrlR) TractorStop
  , keyDown   KeyBackspace Bankrupt
  ]

-- * Ship controls / mouse

playerMouse :: Bool -> InputSchema
playerMouse allowRespawn = schema PlayerMouse (shipActionsMouse allowRespawn) playerMouseBindings

shipActionsMouse :: Bool -> Entity -> OrientActions -> SystemW ()
shipActionsMouse allowRespawn ship = \case
  Respawn (winX, winY) ->
    when allowRespawn $ do
      cam <- get global
      let worldXY = windowToWorld cam (winX, winY)
      Actions.playerBankrupt ship
      Actions.playerSpawnShip worldXY

  OrientBurn (winX, winY) -> do
    cam <- get global
    let worldXY = windowToWorld cam (winX, winY)
    ship $= Orient
      { _orientPoint = worldXY
      , _orientBurn  = True
      }

  OrientTurn (winX, winY) -> do
    cam <- get global
    let worldXY = windowToWorld cam (winX, winY)
    ship $= Orient
      { _orientPoint = worldXY
      , _orientBurn  = False
      }

  OrientUpdate (winX, winY) -> do
    cam <- get global
    let worldXY = windowToWorld cam (winX, winY)
    oriented <- exists ship $ Proxy @Orient
    when oriented $
      ship $~ orientPoint .~ worldXY

  OrientStop -> do
    ship $= Not @Orient
    get ship >>= \case
      Orbit ->
        ship $= (Turn 0, Thrust 0)
      TakeOff{} ->
        ship $= Turn 0
      _ ->
        pure ()

playerMouseBindings :: Bindings OrientActions
playerMouseBindings =
  [ mouseToggle LeftButton  OrientBurn   (const OrientStop)
  , mouseToggle RightButton OrientTurn   (const OrientStop)
  , mouseMove               OrientUpdate

  , mouseDown (modShift +> LeftButton) Respawn
  ]

-- * Global UI stuff

ui :: InputSchema
ui = schema UI uiActions uiBindings

uiActions :: Entity -> UiAction -> SystemW ()
uiActions entity = \case
  ExitToMenu -> do
    global $= Pause False
    leaveFor Intro
  SpawnBot (winX, winY) -> do
    cam <- get global
    let worldXY = windowToWorld cam (winX, winY)
    Actions.botSpawnShip worldXY
  PauseResume ->
    entity $~ \(Pause p) -> Pause (not p)
  UIToggle update ->
    entity $~ update

uiBindings :: Bindings UiAction
uiBindings =
  [ keyDown (modShift +> KeyEsc) ExitToMenu

  , keyDown (modShift +> 'P') PauseResume

  , keyDown (modShift +> 'S') $ UIToggle (toggleScore   %~ nsaCycle)
  , keyDown (modShift +> 'M') $ UIToggle (toggleMarkets %~ nsaCycle)
  , keyDown (modShift +> 'T') $ UIToggle (toggleTraces  %~ nsaCycle)
  , keyDown              't'  $ UIToggle (toggleTrajecs %~ nsaCycle)
  , keyDown (modShift +> 'R') $ UIToggle (toggleRings   %~ not)
  , keyDown (modShift +> 'N') $ UIToggle (toggleNavs    %~ nsaCycle)

  , mouseDown (modShift +> RightButton) SpawnBot
  ]
