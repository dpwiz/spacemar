module Scene.Outro
  ( draw
  , drawUI
  , tick
  , input
  ) where

import Scene.Outro.Draw (draw, drawUI)
import Scene.Outro.Input (input)
import Scene.Outro.Tick (tick)
