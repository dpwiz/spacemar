module Main where

import qualified Apecs
import qualified Graphics.Gloss as Gloss

import UI (playUI)
import Scene (draw, drawUI, onInput, onTick)

import qualified Components
import qualified Config
import qualified Scene

main :: IO ()
main = do
  world <- Components.initWorld
  Apecs.runWith world $ do
    Scene.initialize
    playUI
      Config.windowDisplay
      Gloss.black
      Config.windowFPS
      draw
      drawUI
      onInput
      onTick
